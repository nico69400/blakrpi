#!/bin/bash


DIR=`pwd`

echo "Update Rpi and some packages"
sudo apt update && sudo apt upgrade -y
sudo apt install -y cmake screenfetch python3 python3-pip python-pip python3-dev i2c-tools git python3-rpi.gpio libopenjp2-7-dev libjpeg-dev
echo "install pillow"
pip3 install Pillow

echo "Install Screen Drivers"
cd ~
git clone https://github.com/juj/fbcp-ili9341.git
cd fbcp-ili9341
mkdir build
cd build
cmake -DILI9341=ON -DGPIO_TFT_DATA_CONTROL=24 -DGPIO_TFT_RESET_PIN=25 -DSPI_BUS_CLOCK_DIVISOR=6 -DDISPLAY_ROTATE_180_DEGREES=ON -DSTATISTICS=0 ..
make -j
chmod +x fbcp-ili9341

cd $DIR
echo "prepare rc2.d to launch screen on start"
#sudo touch /etc/init.d/Screen
#sudo su - root -c 'echo "#!/bin/bash"> /etc/init.d/Screen'
#sudo su - root -c 'echo "sudo /home/pi/fbcp-ili9341/build/fbcp-ili9341 &" >> /etc/init.d/Screen'
sudo cp Screen /etc/init.d 
sudo chmod +x /etc/init.d/Screen
sudo ln -s /etc/init.d/Screen /etc/rc2.d/S01Screen

sudo cp /etc/modules /etc/modules.ORIG
sudo cp modules /etc/modules


echo "configure keyboard"

echo "..."
#echo "Check first if i2c detect the tca:"
#sudo i2cdetect -y 1
#echo "Verify if you see:"
#echo "30: -- -- -- -- 34 -- -- -- -- -- -- -- -- -- -- --"

sudo cp /etc/default/keyboard /etc/default/keyboard.ORIG
sudo cp keyboard /etc/default/keyboard

sudo cp tca8418.dts /boot/overlays/
cd /boot/overlays/
sudo dtc -@ -I dts -O dtb -o tca8418.dtbo tca8418.dts

echo "please change keyboard layout in /etc/default/keyboard with us & pc105"
sudo cp blakrpi_short.kmap  ..

#sudo echo "sudo loadkeys blakrpi_short.kmap &"> /etc/rc.local


cd ~
git clone https://github.com/adafruit/Adafruit_Python_SSD1306.git
cd Adafruit_Python_SSD1306
sudo python3 setup.py install



#sudo su - root -c 'echo "sudo python3 ~Adafruit_Python_SSD1306/examples/stats.py &"> /etc/rc.local'

cd $DIR
sudo cp /boot/config.txt /boot/config.txt.ORIG
sudo cp config.txt /boot/
sudo cp /etc/rc.local /etc/rc.local.ORIG
sudo cp rc.local /etc/rc.local

echo "change splashscreen"
sudo cp /usr/share/plymouth/themes/pix/splash.png /usr/share/plymouth/themes/pix/splash.png.ORIG
sudo cp splashscreen_BlakRPI.png /usr/share/plymouth/themes/pix/splash.png

echo "Reboot"
sudo reboot
